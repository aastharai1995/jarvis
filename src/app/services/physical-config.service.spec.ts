import { TestBed } from '@angular/core/testing';

import { PhysicalConfigService } from './physical-config.service';

describe('PhysicalConfigService', () => {
  let service: PhysicalConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhysicalConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
