import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PhysicalConfigService {
  url="";
  token="";
  constructor(private http:HttpClient) { }
  getconfigdata(val1:string, val2:string){
    this.url=val1
    this.token=val2
    return this.http.get(this.url, { 
      headers: new HttpHeaders({'Authorization': 'Bearer ' + this.token,'Connection':'keep-alive','Content-Type':'application/json'}), 
      responseType: 'text' as const });
  }
  setconfigdata(val:string){
    this.url=val
    return this.http.get(this.url);
  }
}
